package suyundukov.rm;

import suyundukov.rm.PivotStrategies.PivotStrategy;
import suyundukov.rm.SortStrategies.SortStrategy;

import java.util.Comparator;

public class Sorter<T> {

    private SortStrategy sortStrategy;
    private PivotStrategy pivotStrategy;
    private Comparator<T> comparator;

    public Builder newBuilder() {
        return new Sorter().new Builder();
    }

    public T[] doSort(T[] array) {
        sortStrategy.sort(array, comparator, pivotStrategy);
        return array;
    }

    public class Builder {

        private Builder() {
        }

        public Builder setSortStrategy(SortStrategy sortStrategy) {
            Sorter.this.sortStrategy = sortStrategy;
            return this;
        }

        public Builder setPivotStrategy(PivotStrategy pivotStrategy) {
            Sorter.this.pivotStrategy = pivotStrategy;
            return this;
        }

        public Sorter build(Comparator<T> comparator) {
            Sorter.this.comparator = comparator;
            return Sorter.this;
        }
    }
}
