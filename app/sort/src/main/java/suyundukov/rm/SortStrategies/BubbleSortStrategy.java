package suyundukov.rm.SortStrategies;

import suyundukov.rm.PivotStrategies.PivotStrategy;

import java.util.Comparator;

public class BubbleSortStrategy implements SortStrategy {

    private Comparator comparator;
    private PivotStrategy pivotStrategy;

    @Override
    public void sort(Object[] array, Comparator comparator, PivotStrategy pivotStrategy) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i; j < array.length - 1; j++) {
                if (comparator.compare(array[j], array[j - 1]) > 0) {
                    Object temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }
    }
}
