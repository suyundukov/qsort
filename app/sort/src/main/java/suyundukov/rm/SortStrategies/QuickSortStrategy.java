package suyundukov.rm.SortStrategies;

import suyundukov.rm.PivotStrategies.PivotStrategy;

import java.util.Comparator;

public class QuickSortStrategy implements SortStrategy {

    private Comparator comparator;
    private PivotStrategy pivotStrategy;

    @Override
    public void sort(Object[] t, Comparator comparator, PivotStrategy pivotStrategy) {
        this.comparator = comparator;
        this.pivotStrategy = pivotStrategy;
        quickSort(t, 0, t.length - 1);
    }

    private void quickSort(Object[] array, Integer startIndex, Integer endIndex) {
        if (startIndex >= endIndex)
            return;

        Integer middle = pivotStrategy.getPivot(startIndex, endIndex);
        Object supportElement = array[middle];

        Integer i = startIndex;
        Integer j = endIndex;

        while (i <= j) {
            while (comparator.compare(array[i], supportElement) <= 0) {
                i++;
            }
            while (comparator.compare(array[j], supportElement) >= 0) {
                j--;
            }
            if (i <= j) {
                Object temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
        }
        if (startIndex < j)
            quickSort(array, startIndex, j);
        if (endIndex > i)
            quickSort(array, i, endIndex);
    }
}
