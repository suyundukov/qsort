package suyundukov.rm.SortStrategies;

import suyundukov.rm.PivotStrategies.PivotStrategy;

import java.util.Comparator;

public interface SortStrategy<T> {
    public void sort(T[] t, Comparator<T> comparator, PivotStrategy pivotStrategy);
}
