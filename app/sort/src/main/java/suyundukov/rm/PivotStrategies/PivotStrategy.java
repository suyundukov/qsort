package suyundukov.rm.PivotStrategies;

public interface PivotStrategy<T> {

    public Integer getPivot(Integer startIndex, Integer endIndex);
}