package suyundukov.rm.PivotStrategies;

public class MiddleElementPivotStrategy implements PivotStrategy {
    @Override
    public Integer getPivot(Integer startIndex, Integer endIndex) {
        return startIndex + (endIndex - startIndex) / 2;
    }
}
